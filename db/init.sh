#!/bin/bash
set -e

psql "dbname=$DB_NAME user=$POSTGRES_USER password=$POSTGRES_PASSWORD" -f /db-init-data/protopixel.sql
