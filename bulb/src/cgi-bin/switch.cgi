#!/bin/bash
echo "Content-type: text/html"
echo ""
echo ""
FILE=/data/bulb-status
if [ ! -f "$FILE" ]; then
    echo 0 > $FILE
fi
status=$(cat $FILE)
if [ $status = "0" ]; then
    echo 1 > $FILE
else
    echo 0 > $FILE
fi
status=$(cat $FILE)
echo "$status"
echo ""
exit 0