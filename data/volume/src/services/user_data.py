from lib import db
import hashlib

#TODO reutilizar conexiones

def get_user_data(userid):
    con = db.get_connection()
    cur = con.cursor()
    query = """SELECT name, 
                      type,
                      serial
	             FROM protopixel.data
                 WHERE rel_user=%(userid)s
                      """
    cur.execute(query, ({
        'userid': userid,
    }))
    res = cur.fetchall()
    res = list(map(lambda x: {
        'Name': x[0],
        'Type': x[1],
        'Serial': x[2],
    }, res))
    return res

def get_remote_plan(userid):
    con = db.get_connection()
    cur = con.cursor()
    query = """SELECT remote_plan
	             FROM protopixel.user
                 WHERE id=%(userid)s
                      """
    cur.execute(query, ({
        'userid': userid,
    }))
    res = cur.fetchone()
    return res[0]