from flask import Flask, request, jsonify
#import jwt
import os
from services import user_data

JWT_SECRET = 'protopixel'

app = Flask(__name__)

# hace falta autorizar?


@app.route('/api/v0/user/<user_id>/devices', methods=['GET'])
def data(user_id):
    #token = request.headers.get('Authorization')
    # if token is None:
    # return 'missing Authorization header', 400

    # try:
    #payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
    # except:
    # return 'invalid token', 400

    data_res = user_data.get_user_data(user_id)
    return jsonify({
        'data': data_res
    })

# hace falta autorizar?


@app.route('/api/v0/user/<user_id>/remote_plan', methods=['GET'])
def remote_plan(user_id):
    #token = request.headers.get('Authorization')
    # if token is None:
    # return 'missing Authorization header', 400

    # try:
    #payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
    # except:
    # return 'invalid token', 400

    data_res = user_data.get_remote_plan(user_id)
    return jsonify({
        'data': {
            'active': data_res
        }
    })
