import socket
import asyncio
import threading

HOST = '0.0.0.0'  # Standard loopback interface address (localhost)
loop = asyncio.new_event_loop()

def create_server(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, port))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                conn.sendall(data)

def create_tunnel():
    t = threading.Thread(target=create_server, args=(9999,))
    t.start()
    return 'done'
        
