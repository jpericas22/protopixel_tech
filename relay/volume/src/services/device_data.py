from lib import db
import hashlib

#TODO reutilizar conexiones

def get_device_data(device_id, user_id):
    con = db.get_connection()
    cur = con.cursor()
    query = """SELECT tunnel_open, 
                      last_connection,
                      rel_user
	             FROM protopixel.data
                 WHERE serial=%(device_id)s
                      """
    cur.execute(query, ({
        'device_id': device_id,
    }))
    res = cur.fetchone()
    if res[2] != user_id:
        raise BaseException('Unauthorized')
    
    res_obj = {
        'connected': res[0]
    }

    #se tiene que comprobar que el tunnel sigue abierto
    if res[0]:
        res_obj['status_report'] = 'No errors'
    else:
        res_obj['last_connection'] = res[1]
    
    return res_obj