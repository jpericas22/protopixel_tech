from flask import Flask, request, jsonify
import jwt
import os
from services import device_data, tunnel

JWT_SECRET = 'protopixel'

app = Flask(__name__)


@app.route('/api/v0/product/<product_id>/status', methods=['GET'])
def data(product_id):
    token = request.headers.get('Authorization')
    if token is None:
        return 'missing Authorization header', 400

    try:
        payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
    except:
        return 'invalid token', 400

    data_res = device_data.get_device_data(product_id, payload['id'])
    return jsonify({
        'data': data_res
    })

@app.route('/api/v0/create_tunnel/<product_id>', methods=['POST'])
def create_tunnel(product_id):
    token = request.headers.get('Authorization')
    if token is None:
        return 'missing Authorization header', 400

    try:
        payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
    except:
        return 'invalid token', 400

    if not payload['remote_plan']:
        return 'user has no remote permission', 403
    
    tunnel.create_tunnel(9999)



