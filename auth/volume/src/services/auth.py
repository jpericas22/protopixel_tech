from lib import db
import hashlib
import jwt

JWT_SECRET = 'protopixel'
#TODO reutilizar conexiones

def salt(username, password):
    con = db.get_connection()
    cur = con.cursor()
    query = """SELECT salt
               FROM protopixel.user
              WHERE username=%(username)s
                      """
    cur.execute(query, ({
        'username': username,
    }))
    res = cur.fetchone()
    if res is None:
        raise BaseException('user not found')
    salt = res[0]
    cur.close()
    return hashlib.sha256(password.encode()+salt.encode()).hexdigest()


def login(username, password):
    hashed = salt(username, password)
    con = db.get_connection()
    cur = con.cursor()
    query = """SELECT id,
                      remote_plan
               FROM protopixel.user
              WHERE username=%(username)s
                AND password=%(password)s
                      """
    cur.execute(query, ({
        'username': username,
        'password': hashed
    }))
    res = cur.fetchone()
    if res is None:
        raise BaseException('unauthorized')
    return {
        'id': res[0],
        'remote_plan': res[1]
    }

#no sigue el formato de firebase
def build_jwt(data):
    return jwt.encode(data, JWT_SECRET, algorithm="HS256")

def decode_jwt(token):
    return jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
