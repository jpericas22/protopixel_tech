from flask import Flask, request
from services import auth
import os

app = Flask(__name__)

@app.route('/login', methods=['POST'])
def login():
    data = request.json
    if 'username' not in data or 'password' not in data:
        return 'malformed json', 400
    
    try:
        payload = auth.login(data['username'], data['password'])
    except:
        return 'forbidden', 403

    #return jwt token
    #se tendria que usar un set-cookie y un response en json
    return auth.build_jwt(payload)
