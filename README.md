build with "docker-compose build" on root dir  
run with "docker-compose run" on root dir  
  
SERVICES  
  
bulb  
    - add bulb.protopixel.io to /etc/hosts  
    - bulb.protopixel.io:8081  
    - GET /cgi-bin/switch.cgi  
    - GET /cgi-bin/status.cgi  
auth  
    - localhost:8082  
    - /login: post JSON with "username" and "password" keys  
db  
    - postgresql running on localhost:8085  
data  
    - localhost:8086  
    - GET /api/v0/user/<user_id>/devices  
    - GET /api/v0/user/<user_id>/remote_plan  
relay  
    - localhost:8084  
    - POST /api/v0/product/<product_id>/status  
    - POST /api/v0/create_tunnel/<product_id>  
